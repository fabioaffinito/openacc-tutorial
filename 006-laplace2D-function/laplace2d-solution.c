#include <math.h>
#include <string.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#ifdef _OPENACC
#include <openacc.h>
#endif

#define NN 4096
#define NM 4096

void update_A(int n, int m, double *error);
void copy_A(int n, int m);

double A[NN][NM];
double Anew[NN][NM];

int main(int argc, char *argv[]) {

    int n = NN;
    int m = NM;
    int iter_max = 100;
    double tol = 1.0e-6;

    double error = 1.0;
    int iter = 0;
    int i,j;
    struct timeval temp_1, temp_2;
    double ttime=0.;

#include "../select_device.h"
    
    memset(A, 0, n * m * sizeof(double));
    memset(Anew, 0, n * m * sizeof(double));
        
    for (j = 0; j < n; j++)
    {
        A[j][0]    = 1.0;
        Anew[j][0] = 1.0;
    }
    
    printf("Jacobi relaxation Calculation: %d x %d mesh\n", n, m);
    
    gettimeofday(&temp_1, (struct timezone*)0);
    
#pragma acc data copy(A), create(Anew)
    while ( error > tol && iter < iter_max )
    {
        update_A(n,m,&error);
        
        copy_A(n,m);
    
        iter++;

        if(iter % 10 == 0) {
#pragma acc update host(A[2:1][2:1])
// work-around for CAPS 3.4.1, update the entire variable instead of the needed section
//#pragma acc update host(A)
          printf("%5d, %0.8lf, %0.6f \n", iter, error, A[2][2]);
        }
    }

    gettimeofday(&temp_2, (struct timezone*)0);
    ttime = 0.000001*((temp_2.tv_sec-temp_1.tv_sec)*1.e6+(temp_2.tv_usec-temp_1 .tv_usec));

    printf("Elapsed time (s) = %.2lf\n", ttime);
    printf("Stopped at iteration: %u\n", iter);

    return 0;

}

void update_A(int n, int m, double *error)
{
double error_loc = 0.0;
#pragma acc parallel loop collapse(2) reduction(max: error_loc)
   for( int j = 1; j < n-1; j++) {
       for( int i = 1; i < m-1; i++ ) {
           Anew[j][i] = 0.25 * ( A[j][i+1] + A[j][i-1]
                               + A[j-1][i] + A[j+1][i]);
           error_loc = fmax( error_loc, fabs(Anew[j][i] - A[j][i]));
       }
   }
*error = error_loc ; 
}

void copy_A(int n, int m)
{
#pragma acc parallel loop collapse(2)
   for( int j = 1; j < n-1; j++) {
       for( int i = 1; i < m-1; i++ ) {
           A[j][i] = Anew[j][i];    
       }
   }
}
