#!/bin/bash
#SBATCH -N 1 
#SBATCH --ntasks-per-node=1 
#SBATCH --cpus-per-task=8 
#SBATCH --gres=gpu:1
#SBATCH --mem=83GB 
#SBATCH --time=0:10:00  
#SBATCH --account=cin_staff # account number
#SBATCH --partition=gll_usr_gpuprod # partition to be used

module load profile/global pgi/19.10--binary

DIR=001-laplace2D-accparallel

cd $DIR
./laplace2d_acc_pgi

