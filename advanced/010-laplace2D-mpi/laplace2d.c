#define MAX(A,B) (((A) > (B)) ? (A) : (B))
#define ABS(A)   (((A) <  (0)) ? (-A): (A))

#ifdef _OPENACC
#include "openacc.h"
#endif
// solves 2D Laplace equation using a relaxation scheme

#include <stdio.h>
#include <math.h>
#include <float.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <mpi.h>

int main(int argc, char *argv[]) {
   
   unsigned n, stride_x, stride_y, maxIter, i, j, iter = 0;
   double tol, var = DBL_MAX, top = 100.0;
   double * T, * Tnew;
#ifdef _OPENACC
#define NGPU_PER_NODE 2
   int mygpu, myrealgpu, num_devices;
#endif
   int itemsread;
   double endTime;
   FILE *fout;
// MPI
   int rank, nprocs, required, provided, tag=201;
   MPI_Status status;
   MPI_Comm cartesianComm;
   int cart_rank, cart_dims[2], cart_coord[2];
   int cart_reorder, cart_periods[2];
   int mymsize_x, mymsize_y, res, mystart_x, mystart_y;
   double * buffer_s_rl,* buffer_s_lr;
   double * buffer_s_tb,* buffer_s_bt;
   double * buffer_r_rl,* buffer_r_lr;
   double * buffer_r_tb,* buffer_r_bt;
   int source_rl, dest_rl, source_lr, dest_lr;
   int source_tb, dest_tb, source_bt, dest_bt;
   double myvar=1.0;
   int ierr;
   char filename[14];
   int output=0;
#ifdef _OPENACC
   acc_device_t my_device_type;
#endif

   ierr = MPI_Init(&argc,&argv); 
   ierr = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
   ierr = MPI_Comm_size(MPI_COMM_WORLD, &nprocs);

#ifdef _OPENACC
   if(rank == 0) {
       fprintf(stderr,"MPI+OpenACC Usage. Please make sure that:\n ");
       fprintf(stderr,"(a) total number of processes is <= or multiple of NGPU_PER_NODE\n");
       fprintf(stderr,"(b) you are executing mpirun -npernode NGPUPERNODE <name executable>\n");
   }
   if(nprocs > NGPU_PER_NODE && nprocs%NGPU_PER_NODE != 0) {
      fprintf(stderr,"Error in process set: check number of processes\n");
      MPI_Finalize();
      exit(-1);
   }

#ifdef CAPS
   mygpu = rank%NGPU_PER_NODE+1;
   my_device_type = acc_device_cuda;
#elif PGI
   mygpu = rank%NGPU_PER_NODE;
   my_device_type = acc_device_nvidia;
#endif
   acc_set_device_type(my_device_type) ;
   num_devices = acc_get_num_devices(my_device_type) ; 
   fprintf(stderr,"Number of devices available: %d \n ",num_devices);
   acc_set_device_num(mygpu,my_device_type); 
   fprintf(stderr,"Rank : %d tries to use GPU: %d \n",rank,mygpu);
   myrealgpu = acc_get_device_num(my_device_type); 
   fprintf(stderr,"Rank : %d is using GPU: %d \n",rank,myrealgpu);
   if(mygpu != myrealgpu) {
     fprintf(stderr,"I cannot use the requested GPU: %d\n",mygpu);
     exit(1);
   }
#endif

   if ( rank == 0 ) {
#ifdef FIXED_INPUT
      n=4096; maxIter=100; tol=0.0001;
#else
      fprintf(stderr,"Enter mesh size, max iterations and tolerance: \n");
      itemsread = scanf("%u ,%u ,%lf", &n, &maxIter, &tol);
      if (itemsread!=3) {
         fprintf(stderr, "Input error!\n");
         MPI_Finalize();
         exit(-1);
      }
#endif
   }

    // broadcast input parameters (n,maxIter,tol) from process 0 to others
    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&maxIter, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&tol, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    // creating a new communicator with a 2D cartesian topology 
    cart_dims[0] = 0 ; cart_dims[1] = 0 ;
    MPI_Dims_create(nprocs,2,cart_dims);
    cart_periods[0] = 0 ; cart_periods[1] = 0 ;
    cart_reorder = 0;
    MPI_Cart_create(MPI_COMM_WORLD, 2, cart_dims, 
      cart_periods, cart_reorder, &cartesianComm );
    MPI_Comm_rank(cartesianComm, &cart_rank);
    MPI_Cart_coords(cartesianComm, cart_rank, 2, cart_coord);

    // calculating problem size/process
    res = n% cart_dims[0];
    mymsize_x = n / cart_dims[0];
    mystart_x = mymsize_x * cart_coord[0];
    if (cart_coord[0] > cart_dims[0] -1-res) {
       mymsize_x = mymsize_x + 1;
       mystart_x = mystart_x + cart_coord[0] - (cart_dims[0] -res);
    }
    res = n% cart_dims[1];
    mymsize_y = n / cart_dims[1];
    mystart_y = mymsize_y * cart_coord[1];
    if (cart_coord[1] > cart_dims[1] -1-res) {
       mymsize_y = mymsize_y + 1;
       mystart_y = mystart_y + cart_coord[1] - (cart_dims[1] -res);
    }
//    printf("I am %d of %d processes\n",rank,nprocs);
//    printf("mymsize_x:  %d\n",mymsize_x);
//    printf("mymsize_y:  %d\n",mymsize_y);

    stride_x = mymsize_x + 2;
    stride_y = mymsize_y + 2;
    // allocating memory for T , Tnew and buffers, calloc initializes to zero...
    T           = (double*) calloc(stride_x*stride_y, sizeof(*T));
    Tnew        = (double*) calloc(stride_x*stride_y, sizeof(*T));
    buffer_s_rl = (double*) calloc(mymsize_y, sizeof(*T));
    buffer_s_lr = (double*) calloc(mymsize_y, sizeof(*T));
    buffer_s_tb = (double*) calloc(mymsize_x, sizeof(*T));
    buffer_s_bt = (double*) calloc(mymsize_x, sizeof(*T));
    buffer_r_rl = (double*) calloc(mymsize_y, sizeof(*T));
    buffer_r_lr = (double*) calloc(mymsize_y, sizeof(*T));
    buffer_r_tb = (double*) calloc(mymsize_x, sizeof(*T));
    buffer_r_bt = (double*) calloc(mymsize_x, sizeof(*T));

    // boundary conditions 
    if ( cart_coord[0] == cart_dims[0]-1)  {
      for(j = 0;j<=mymsize_y+1;j++) {
         T[stride_y*(mymsize_x+1)+j] = (mystart_y+j)*top/(n+1);
      }
    }
    if ( cart_coord[1] == cart_dims[1]-1)  {
      for(i = 0;i<=mymsize_x+1;i++) {
         T[stride_y*i+mymsize_y+1] = (mystart_x+i)*top/(n+1);
      }
    }
   
    for(i = 0; i<=mymsize_x+1; i++) 
       for(j = 0; j<=mymsize_y+1; j++) 
          Tnew[i*stride_y+j] = T[i*stride_y+j] ;

   // calculating source/dest neighbours (right->left)
   MPI_Cart_shift(cartesianComm, 0, -1, &source_rl, &dest_rl);
   // calculating source/dest neighbours (left->right)
   MPI_Cart_shift(cartesianComm, 0,  1, &source_lr, &dest_lr);
   // calculating source/dest neighbours (top->bottom)
   MPI_Cart_shift(cartesianComm, 1, -1, &source_tb, &dest_tb);
   // calculating source/dest neighbours (bottom->top)
   MPI_Cart_shift(cartesianComm, 1,  1, &source_bt, &dest_bt);

   fprintf(stderr,"Starting iterations \n");

   time_t startTime = clock();

#pragma acc data copy(T[0:stride_x*stride_y]), create(Tnew[0:stride_x*stride_y]), \
create(buffer_s_rl[0:mymsize_y]), \
create(buffer_s_lr[0:mymsize_y]), \
create(buffer_s_tb[0:mymsize_x]), \
create(buffer_s_bt[0:mymsize_x]), \
create(buffer_r_rl[0:mymsize_y]), \
create(buffer_r_lr[0:mymsize_y]), \
create(buffer_r_tb[0:mymsize_x]), \
create(buffer_r_bt[0:mymsize_x])
   while(var > tol && iter <= maxIter) {
      ++iter;
      var = 0.0;
      myvar = 0.0;
#pragma acc kernels
#pragma acc loop independent
      for(j = 1; j<=mymsize_y; j++) 
         buffer_s_rl[j-1] = T[stride_y+j];
#pragma acc kernels
#pragma acc loop independent
      for(j = 1; j<=mymsize_y; j++) 
         buffer_s_lr[j-1] = T[mymsize_x*stride_y+j];
#pragma acc kernels
#pragma acc loop independent
      for(i = 1; i<=mymsize_x; i++) 
         buffer_s_tb[i-1] = T[stride_y*i+1];
#pragma acc kernels
#pragma acc loop independent
      for(i = 1; i<=mymsize_x; i++) 
         buffer_s_bt[i-1] = T[stride_y*i+mymsize_y];
#pragma acc update host(buffer_s_rl[0:mymsize_y])
#pragma acc update host(buffer_s_lr[0:mymsize_y])
#pragma acc update host(buffer_s_tb[0:mymsize_x])
#pragma acc update host(buffer_s_bt[0:mymsize_x])
        //--- exchange boundary data with neighbours (right->left)
      MPI_Sendrecv(buffer_s_rl, mymsize_y, MPI_DOUBLE, dest_rl, tag,   
                   buffer_r_rl, mymsize_y, MPI_DOUBLE, source_rl, tag, 
                   cartesianComm, &status);
      if(source_rl >= 0) {
#pragma acc update device(buffer_r_rl[0:mymsize_y])
#pragma acc kernels
#pragma acc loop independent
        for(j = 1; j<=mymsize_y; j++) 
           T[stride_y*(mymsize_x+1)+j] = buffer_r_rl[j-1];
      }

        //--- exchange boundary data with neighbours (left->right)
        MPI_Sendrecv(buffer_s_lr, mymsize_y, MPI_DOUBLE, dest_lr, tag+1,   
                     buffer_r_lr, mymsize_y, MPI_DOUBLE, source_lr, tag+1, 
                     cartesianComm, &status);
      if(source_lr >= 0) {
#pragma acc update device(buffer_r_lr[0:mymsize_y])
#pragma acc kernels
#pragma acc loop independent
        for(j = 1; j<=mymsize_y; j++) 
           T[j] = buffer_r_lr[j-1];
      }

        //--- exchange boundary data with neighbours (top->bottom)
        MPI_Sendrecv(buffer_s_tb, mymsize_x, MPI_DOUBLE, dest_tb, tag+2,   
                     buffer_r_tb, mymsize_x, MPI_DOUBLE, source_tb, tag+2, 
                     cartesianComm, &status);
      if(source_tb >= 0) {
#pragma acc update device(buffer_r_tb[0:mymsize_x])
#pragma acc kernels
#pragma acc loop independent
        for(i = 1; i<=mymsize_x; i++) 
           T[stride_y*i+mymsize_y+1] = buffer_r_tb[i-1];
      }

        //--- exchange boundary data with neighbours (bottom->top)
        MPI_Sendrecv(buffer_s_bt, mymsize_x, MPI_DOUBLE, dest_bt, tag+3,   
                     buffer_r_bt, mymsize_x, MPI_DOUBLE, source_bt, tag+3, 
                     cartesianComm, &status);
      if(source_bt >= 0) {
#pragma acc update device(buffer_r_bt[0:mymsize_x])
#pragma acc kernels
#pragma acc loop independent
        for(i = 1; i<=mymsize_x; i++) 
           T[stride_y*i] = buffer_r_bt[i-1];
      }

#pragma acc parallel loop collapse(2) reduction(max:myvar)
      for (i=1; i<=mymsize_x; ++i) {
         for (j=1; j<=mymsize_y; ++j) {         
            Tnew[i*stride_y+j] = 0.25*( T[(i-1)*stride_y+j] + T[(i+1)*stride_y+j] 
                                + T[i*stride_y+(j-1)] + T[i*stride_y+(j+1)] );
#ifdef _OPENACC
            myvar = fmax(myvar, fabs(Tnew[i*stride_y+j] - T[i*stride_y+j]));
#else
            myvar = MAX(myvar, ABS(Tnew[i*stride_y+j] - T[i*stride_y+j]));
#endif
         }
     }

#pragma acc parallel loop collapse(2) 
      for (i=1; i<=mymsize_x; ++i)
         for (j=1; j<=mymsize_y; ++j) {         
            T[i*stride_y+j] = Tnew[i*stride_y+j];
         }

      MPI_Allreduce(&myvar, &var, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

      if ( rank == 0 ) {
         if (iter%10 == 0) fprintf(stderr,"iter: %8u, variation = %12.4lE\n", iter, var);
      }
   }

    endTime = (clock() - startTime) / (double) CLOCKS_PER_SEC;

   if ( rank == 0 ) {
      printf("Elapsed time (s) = %.2lf\n", endTime);
      printf("Mesh size: %u\n", n);
      printf("Stopped at iteration: %u\n", iter);
      printf("Maximum error: %lE\n", var);
   }
   // saving results to file
   if(output == 1) {
      sprintf(filename, "results_%d", rank);
      fout = fopen(filename, "w");
      if (fout == NULL) {
         perror("results");
         exit(-1);
      }
      for (i=1; i<=mymsize_x; ++i)
         for (j=1; j<=mymsize_y; ++j)
            fprintf(fout, "%8u %8u %.14lE\n",mystart_x+i, mystart_y+j, T[i*stride_x+j]);
      fclose(fout);
   }
   free(T);
   free(Tnew);

   ierr = MPI_Finalize();
   return 0;
}
