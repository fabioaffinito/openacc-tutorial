#include <math.h>
#include <string.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#ifdef _OPENACC
#include <openacc.h>
#endif

#include <vector>
#include <iostream>

#define NN 4096
#define NM 4096

#ifdef _OPENACC
#define IDX(i,j) ((i)*(NM)+(j))
#endif

using namespace std;

class field {
  int n;
  int m;
  vector<double> vec;
public:
  field(int nn, int mm) : n(nn), m(mm) { vec.assign(nn*mm,0.); }
  double& operator() (int i, int j) { return vec[m*i+j]; }
  double* operator() () { return &(vec[0]); }
};

int main(int argc, char *argv[]) {

    int n = NN;
    int m = NM;
    int iter_max = 100;
    double tol = 1.0e-6;

    double error = 1.0;
    int iter = 0;
    int i,j;
    struct timeval temp_1, temp_2;
    double ttime=0.;
    double *A_p, *Anew_p;

#include "../select_device.h"

    field A(n,m);
    field Anew(n,m);
        
    for (j = 0; j < n; j++) { 
        A(j,0)   = 1.0;
        Anew(j,0)= 1.0;
    }
    
    cout << "Jacobi relaxation Calculation: " << n << " x " << m << endl; 

    A_p = A();
    Anew_p = Anew();
    
    gettimeofday(&temp_1, (struct timezone*)0);
    
#pragma acc data copy(A_p[0:NN*NM]), create(Anew_p[0:NN*NM]) 
{
    while ( error > tol && iter < iter_max )
    {
        error = 0.0;

#pragma acc parallel loop collapse(2) reduction(max: error)
        for( j = 1; j < n-1; j++) {
            for( i = 1; i < m-1; i++ ) {
#ifdef _OPENACC
                Anew_p[IDX(j,i)] = 0.25 * ( A_p[IDX(j,i+1)] + A_p[IDX(j,i-1)]
                                          + A_p[IDX(j-1,i)] + A_p[IDX(j+1,i)]);
                error = fmax( error, fabs(Anew_p[IDX(j,i)] - A_p[IDX(j,i)]));
#else
                Anew(j,i) = 0.25 * ( A(j,i+1) + A(j,i-1)
                                   + A(j-1,i) + A(j+1,i));
                error = fmax( error, fabs(Anew(j,i) - A(j,i)));
#endif
            }
        }

#pragma acc parallel loop collapse(2)
        for( j = 1; j < n-1; j++) {
            for( i = 1; i < m-1; i++ ) {
#ifdef _OPENACC
                A_p[IDX(j,i)] = Anew_p[IDX(j,i)];
#else
                A(j,i) = Anew(j,i);
#endif
            }
        }
        
        iter++;

        if(iter % 10 == 0) printf("%5d, %0.8lf\n", iter, error);
    }
}

    gettimeofday(&temp_2, (struct timezone*)0);
    ttime = 0.000001*((temp_2.tv_sec-temp_1.tv_sec)*1.e6+(temp_2.tv_usec-temp_1 .tv_usec));

    cout << "Elapsed time (s): " << ttime << endl; 
    cout << "Stopped at iteration: " << iter << endl; 

    return 0;

}
