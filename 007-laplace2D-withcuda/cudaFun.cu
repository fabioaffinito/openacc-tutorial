   extern "C" void cudaFun(double **A, double **Anew, int n, int m)  {

       cudaMemcpy((double*)A,(double*)Anew,m*n*sizeof(A[0][0]),cudaMemcpyDeviceToDevice);

   }
