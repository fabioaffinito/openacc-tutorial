#!/bin/bash
#PBS -N laplace_mpiacc
#PBS -o job.out
##PBS -e job.err
#PBS -j oe
#PBS -l walltime=00:10:00
#PBS -l select=1:ncpus=2:ngpus=2:mpiprocs=2
#PBS -q debug
#PBS -A train_scA2015
#PBS -q R1607008

module load profile/advanced 
module load pgi/14.10--binary openmpi/1.6.5--pgi--14.1

cd $PBS_O_WORKDIR

DIR=010-laplace2D-mpi

cd $DIR
mpirun -np 2 ./laplace2d_acc_pgi
