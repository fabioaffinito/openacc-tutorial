#!/bin/bash
#SBATCH -N 1 
#SBATCH --ntasks-per-node=1 
#SBATCH --cpus-per-task=8 
#SBATCH --gres=gpu:1
#SBATCH --mem=83GB 
#SBATCH --time=0:10:00  
#SBATCH --account=cin_staff # account number
#SBATCH --partition=gll_usr_gpuprod # partition to be used

module load profile/global pgi/19.10--binary

DIR=000-laplace2D-openmp

cd $DIR

export OMP_NUM_THREADS=1
./laplace2d_omp_pgi

#export OMP_NUM_THREADS=2
#./laplace2d_omp_pgi

#export OMP_NUM_THREADS=4
#./laplace2d_omp_pgi

#export OMP_NUM_THREADS=8
#./laplace2d_omp_pgi
